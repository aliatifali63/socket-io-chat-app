// const moment = require('moment');

// function formatMessage(username,text){
//     return{
//         username,
//         text,
//         time: moment().format('h:mm a')//here a is for am/pm
//     };
// }

// module.exports = formatMessage;

const moment = require('moment');

function formatMessage(username, text) {
  return {
    username,
    text,
    time: moment().format('h:mm a')//a means am/pm here
  };
}

module.exports = formatMessage;
